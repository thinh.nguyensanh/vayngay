@extends('layouts.layout-archive')

@section('content')
    @component('components.topic-title', [
    'title' => App::title()
    ])
    @endcomponent

    @if (!have_posts())
        <div class="alert alert-warning">
            {{ __('Sorry, no results were found.', 'sage') }}
        </div>
        {!! get_search_form(false) !!}
    @endif

    <div class="list-posts">
        <div class="row py-4">
            @while (have_posts())
            @php
                the_post();
                $id = get_the_ID();
                $featured_image_id = get_post_thumbnail_id($id);
                $category_detail = get_the_category($id);
            @endphp
            <div class="col-md-6 col-12 px-0">
                @component('components.post-item-vertical', [
                    'name' => get_the_title(),
                    'excerpt' => wp_trim_words( get_the_content(), 40, '...' ),
                    'url' => get_the_permalink(),
                    'image' => $featured_image_id ? wp_get_attachment_url($featured_image_id) : '',
                    'views' => get_field('views', $id),
                    'category_name' => !empty($category_detail) ? $category_detail[0]->name : '',
                    'post_date' => date("d/m/Y", strtotime($post->post_date)),
                ])
                @endcomponent
            </div>
            @endwhile
        </div>
    </div>
    {{\App\Controllers\App::pagination_link()}}
@endsection
