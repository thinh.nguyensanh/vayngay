<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/13/19
 * Time: 10:53 AM
 */
?>

<!-- Swiper -->
<div class="home-slider">
  @foreach ($list_slider as $slider)
    <div class="slide-item">
      <div class="slide-inner" style="background-image: url('{{$slider['image']}}');">
      </div>
    </div>
  @endforeach

</div>
