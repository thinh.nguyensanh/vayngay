<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/15/19
 * Time: 11:45 AM
 */
?>

<div class="post-item post-horizontal">
  <a href="{{$url}}}">
    <div class="post-image">
      <img src="{{$image}}">
    </div>
    <div class="post-content">
      <div class="post-title">
        <h4>{{$name}}</h4>
      </div>
      <div class="post-meta">
      <span class="post-date">
        {{$post_date}}
      </span>
      </div>
    </div>
  </a>
</div>

