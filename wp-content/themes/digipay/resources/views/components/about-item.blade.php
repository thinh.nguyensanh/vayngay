<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/14/19
 * Time: 10:36 AM
 */
?>

<div class="about-item {{$class}}">
  <div class="about-icon">
    <img src="{{$image}}">
  </div>
  <div class="about-title">
    <h4>{{$title}}</h4>
  </div>
  <div class="about-desc">
    <p>{{$desc}}</p>
  </div>
</div>
