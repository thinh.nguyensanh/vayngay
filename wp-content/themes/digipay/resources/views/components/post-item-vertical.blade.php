<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/15/19
 * Time: 11:45 AM
 */
?>

<div class="post-item post-vertical pb-4">
  <a href="{{$url}}}">
    <div class="post-image">
      <img src="{{$image}}">
      <p>{{$category_name}}</p>
    </div>
    <div class="post-title">
      <h4>{{$name}}</h4>
    </div>
    <div class="post-meta">
      <span class="post-date">
        {{$post_date}}
      </span>
      @if ($views && $views > 0)
        <span class="post-view">
          {{$views}} lượt xem
        </span>
      @endif
    </div>
    @if (isset($excerpt))
    <div class="post-excerpt">
      {{$excerpt}}
    </div>
    @endif
  </a>
</div>
