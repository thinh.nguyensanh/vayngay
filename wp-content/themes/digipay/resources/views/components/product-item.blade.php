<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/13/19
 * Time: 3:14 PM
 */
?>

<style>
  .product-item{
    background: #f6f8fb;
    border-radius: 4px;
    position: relative;
  }

  .product-item.active{
    border: solid 1px #002d58;
  }

  .product-item:hover {
    cursor: pointer;
  }

  .product-item.active:after {
    content: '\f00c';
    font-family: "Font Awesome 5 Free";
    color: #fff;
    font-size: 14px;
    position: absolute;
    padding: 4px 5px 2px 7px;
    top: -16px;
    right: -9px;
    border: 1px solid;
    border-radius: 100%;
    background: #f7d235;
  }
</style>

<div class="product-item {{ isset($class) ? $class : '' }}" data-id="{{$id}}">
  <div class="product-icon">
    <img src="{{$image}}">
  </div>
  <div class="product-title">
    <h4>{{$title}}</h4>
  </div>
</div>
