<!-- Modal -->
<div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="success-modal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="popup-success">
        <div class="popup-success__image">
          <img src="@asset('images/popup-success.png')">
        </div>

        <div class="popup-success_content">
          <p>Chúc mừng bạn đã đăng ký tư vấn vay miễn phí thành công. nhân viên của chúng tôi sẽ liên hệ bạn trong thời gian sớm nhất, vui lòng giữ và bắt máy khi có số lạ gọi đến.
            Chân thành cảm ơn!</p>
          @component('components.button', [
            'type'=> 'button',
            'id' => 'btn-close-popup-success',
            'text' => 'Quay lại trang chủ',
            'class' => 'btn-vn-primary',
            'extra' => 'data-dismiss=modal',
          ])
          @endcomponent
        </div>
      </div>
    </div>
  </div>
</div>
