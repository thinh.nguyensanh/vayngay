<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/13/19
 * Time: 6:14 PM
 */
?>

<button id="{{$id}}" type="{{$type}}" class="ladda-button {{$class}}" {{isset($extra) ? $extra : ''}}>
  <span class="ladda-label">
    {{$text}}
  </span>
</button>
