<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/16/19
 * Time: 3:23 PM
 */
?>

<a class="tag-item" href="{!! get_tag_link($tag->term_id) !!}">
  <span>{{$tag->name}}</span>
</a>
