@extends('layouts.layout-right-sidebar')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  @component('components.topic-title', [
    'title' => App::title()
  ])
  @endcomponent
    @include('partials.content-page')
  @endwhile
@endsection
