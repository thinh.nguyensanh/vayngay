@extends('layouts.layout-right-sidebar')

{{--{{ \App\Controllers\SinglePost::count_view_post() }}--}}

@section('content')
  <div class="single-post">
    @while(have_posts()) @php the_post() @endphp
    @include('partials.content-single-'.get_post_type())
    @endwhile
  </div>
@endsection
