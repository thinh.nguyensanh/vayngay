<footer class="footer" id="section-contact">

  <div class="footer-main">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-12" style="overflow: hidden;">
          <div class="fb-page" data-href="https://www.facebook.com/taichinhcanhanvietnam/?__tn__=%2Cd%3C-R&amp;eid=ARB9Rvy8ZwxDuiLk60I7J8QJY09P_v5k-34-AWgepUWHl5mQVQ0UV72QYRO0BwMuOkM6ItNSQ0cIyh2a" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/taichinhcanhanvietnam/?__tn__=%2Cd%3C-R&amp;eid=ARB9Rvy8ZwxDuiLk60I7J8QJY09P_v5k-34-AWgepUWHl5mQVQ0UV72QYRO0BwMuOkM6ItNSQ0cIyh2a" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/taichinhcanhanvietnam/?__tn__=%2Cd%3C-R&amp;eid=ARB9Rvy8ZwxDuiLk60I7J8QJY09P_v5k-34-AWgepUWHl5mQVQ0UV72QYRO0BwMuOkM6ItNSQ0cIyh2a">Cộng Đồng Tài Chính Cá Nhân Của Người Việt</a></blockquote></div>
        </div>

        <div class="col-md-3 offset-md-1 col-12">
          <div class="contact-info">
            <div class="contact-title">
              <h3>vayngay.vn</h3>
            </div>

            <div class="contact-company">
              <p>Công ty Cổ Phần Giải Pháp Thanh Toán Số</p>
            </div>

            <div class="contact-address">
              <p>Địa chỉ:</p>
              <p>146-148 Cộng Hòa, P.12, Q. Tân Bình, TP.HCM</p>
            </div>

            <div class="contact-social">
              <a href="">
                <img src="@asset('images/facebook-icon.svg')">
              </a>

              <a href="">
                <img src="@asset('images/zalo-icon.svg')">
              </a>
            </div>
          </div>
        </div>

        <div class="col-md-2 col-6">
          <div class="iso">
            <img src="@asset('images/iso-logo@2x.png')">
          </div>
        </div>

        <div class="col-md-3 col-6">
          <div class="row">
            <div class="col-6">
              <div class="nav-footer-menu">
                @if (has_nav_menu('primary_navigation'))
                  {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
                @endif
              </div>
            </div>

            <div class="col-6">
              <div class="nav-footer-menu">
                @if (has_nav_menu('primary_navigation'))
                  {!! wp_nav_menu(['theme_location' => 'extra_navigation', 'menu_class' => 'nav']) !!}
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer-bottom">
    <div class="copyright">
      <p>COPYRIGHT @ 2017 - Công ty Cổ phần Giải Pháp Thanh Toán Số (DigiPay JSC)</p>
    </div>
  </div>

</footer>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3&appId=424757121260058"></script>

