<div class="entry-meta">
  <div class="entry-date">
    <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ date("d.m.Y", strtotime(get_the_date())) }}</time>
  </div>

  @php
    $post_views = get_field('views');
  @endphp

  @if ($post_views && $post_views > 0)
    <div class="entry-views">
      <span>{{$post_views}} lượt xem</span>
    </div>
  @endif
</div>
