<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/13/19
 * Time: 3:09 PM
 */
?>

<div class="container">
  <div class="wrapper-loan">
    <div class="wrapper-title">
      <h3>
        Bạn đang có nhu cầu?
      </h3>
    </div>
    <div class="wrapper-content">
      <div class="list-product">
        <div class="row">
          @foreach ($list_product_item as $index => $product_item)
            @php
              if ($index == 0) {
                $product_item['class'] = 'active';
              }
            @endphp
            <div class="col-6 col-md-6 col-lg-4 py-lg-0 py-3">
              @component('components.product-item', $product_item)
              @endcomponent
            </div>
          @endforeach
        </div>
      </div>

      <div class="form-fill-info">
        <form id="form-prescoring" method="post">
          <div class="form-title">
            <h4>Hãy nhập thông tin</h4>
          </div>

          <div class="form-content">
            <div class="form-group">
              <label>
                Họ và tên
              </label>
              <input type="text" name="fullName" placeholder="Trương Đình Công">
            </div>

            <div class="form-group">
              <label>
                Số điện thoại
              </label>
              <input type="text" name="phoneNumber" placeholder="090 345 xxxx">
            </div>

            <div class="form-group">
              <label>
                Số chứng minh
              </label>
              <input type="text" name="idNumber" placeholder="025 xx xx xx">
            </div>

            <div id="group-address" class="form-group">
              <label>
                Địa chỉ
              </label>

              <select class="single-select" name="address">
                <option value=""></option>
                @foreach ($list_district as $district)
                  <option value="{{$district['code']}}">{{$district['name']}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group" style="text-align: center">
              <input type="hidden" name="project" value="1">
              @component('components.button', [
                'type'=> 'submit',
                'id' => 'btn-loan-reg',
                'text' => 'Đăng ký',
                'class' => 'btn-vn-primary',
                'extra' => 'data-style=expand-right',
              ])
              @endcomponent

            </div>

            <div class="notice notice--check-loan">
              <div class="notice--check-loan__title">
              </div>
              <div class="notice--check-loan__content">
              </div>
            </div>
          </div>
        </form>

        <form id="form-final" method="post" style="display: none">
          <div class="form-content">
            <div class="form-group">
              <label>
                Hình thức vay
              </label>
              <select class="single-select" name="productCode">
                <option value=""></option>
                @foreach ($list_product as $product)
                  <option value="{{$product['code']}}">{{$product['name']}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label>
                Thu nhập
              </label>
              <select class="single-select" name="income">
                <option value=""></option>
                @foreach ($list_income as $income)
                  <option value="{{$income['code']}}">{{$income['name']}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label>
                Khoản vay mong muốn
              </label>
              <select class="single-select" name="loanAmount">
                <option value=""></option>
                @foreach ($list_loan_amount as $loan_amount)
                  <option value="{{$loan_amount['code']}}">{{$loan_amount['name']}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label>
                Thời hạn vay mong muốn
              </label>
              <select class="single-select" name="loanTerm">
                <option value=""></option>
                @foreach ($list_loan_term as $loan_term)
                  <option value="{{$loan_term['code']}}">{{$loan_term['name']}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group" style="text-align: center">
              @component('components.button', [
                'type'=> 'submit',
                'id' => 'btn-loan-final',
                'text' => 'Hoàn tất',
                'class' => 'btn-vn-primary',
                'extra' => 'data-style=expand-right',
              ])
              @endcomponent
            </div>

          </div>
        </form>
      </div>
    </div>
  </div>
</div>
