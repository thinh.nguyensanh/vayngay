<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/15/19
 * Time: 3:00 PM
 */
?>

<div class="wrapper-partner">
  <div class="container">
    <div class="wrapper-title">
      <h3>ĐỐI TÁC</h3>
      <p>Digipay, đơn vị sở hữu Vayngay hiện đang là đối tác giới thiệu sản phẩm của</p>
    </div>

    <div class="wrapper-content">
      <div class="list-partner">
        @foreach ($list_partner as $partner)
          <div class="partner-logo">
            <img src="{{$partner['image']}}">
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
