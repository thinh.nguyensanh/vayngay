<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/14/19
 * Time: 11:55 AM
 */
?>
<div class="wrapper-news">
  <div class="container">
    <div class="wrapper-title">
      <h3>TIN TỨC</h3>
      <p>Cập nhật những thông tin về tài chính cùng Vayngay</p>
    </div>

    <div class="wrapper-content">
      <div class="list-news">
        @foreach ($lasted_posts as $post)
          @component('components.post-item-vertical', $post)
          @endcomponent
        @endforeach
      </div>
      <div class="btn-show-more">
        <a href="{{'/tin-tuc'}}" class="btn-vn-primary">Xem thêm</a>
      </div>
    </div>
  </div>
</div>
