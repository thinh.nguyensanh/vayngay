<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/14/19
 * Time: 10:01 AM
 */
?>

  <div class="wrapper-about">
    <div class="container">
      <div class="wrapper-title wrapper-title-light">
        <h3>Giới thiệu</h3>
        <p>Lý do đặt niềm tin tại Vayngay.vn</p>
      </div>

      <div class="wrapper-content">
          <div class="list-about">
            <div class="row">
              @foreach ($list_about_item as $item)
                <div class="col-12 col-md-6 py-3">
                  @component('components.about-item', $item)
                  @endcomponent
                </div>
              @endforeach
          </div>
          </div>
      </div>
    </div>
  </div>
