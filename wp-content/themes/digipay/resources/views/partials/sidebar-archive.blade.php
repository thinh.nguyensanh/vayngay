<div class="widget-group">
  @component('components.topic-title', [
    'title' => 'Tags'
  ])
  @endcomponent

  <div class="list-tags pt-3">
    @foreach($all_tags as $tag)
      @component('components.tag', [
        'tag' => $tag,
      ])
      @endcomponent
    @endforeach
  </div>
</div>

<div class="widget-group">
  @component('components.topic-title', [
    'title' => 'Mục lục'
  ])
  @endcomponent

  <div class="list-categories">
    @foreach($all_categories as $category)
      <li>
        <a href="{!! get_category_link($category->term_id) !!}">{{$category->name}}</a>
      </li>
    @endforeach
  </div>
</div>
