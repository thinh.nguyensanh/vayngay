<article @php post_class() @endphp>
  <header class="entry-header">
    <img src="{!! wp_get_attachment_url(get_post_thumbnail_id()) !!}">
    @php
      $category_detail = get_the_category();
      foreach ($category_detail as $category) $list_name_category[] = $category->name;
    @endphp
    <h3 class="entry-category">
      {!! implode(',', $list_name_category) !!}
    </h3>
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
    @include('partials.single.entry-meta')
  </header>

  <div class="entry-content">
    @php the_content() @endphp
  </div>

  <footer class="entry-footer">
    @php
      $tags = get_the_tags();
    @endphp
    @if (!empty($tags))
      <div class="entry-tag">
        <p>Tags</p>
        @foreach($tags as $tag)
          @component('components.tag', [
            'tag' => $tag,
          ])
          @endcomponent
        @endforeach
      </div>
    @endif
  </footer>
</article>

<div class="related-post">
  @component('components.topic-title', [
    'title' => 'Bài viết liên quan'
  ])
  @endcomponent

  <div class="related-list-post">
    <div class="row py-5">
      @foreach ($related_posts as $post)
        <div class="col-md-6 col-12 px-0">
          @component('components.post-item-vertical', $post)
          @endcomponent
        </div>
      @endforeach
    </div>
  </div>
</div>
