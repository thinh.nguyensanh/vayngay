<!doctype html>
<html {!! get_language_attributes() !!}>
@include('partials.head')
<body @php body_class() @endphp>
@php do_action('get_header') @endphp
@include('partials.header')
<div class="web-container" role="document">
  <div class="content container my-5">
    <div class="row py-2">
      <main class="main col-md-8 col-12">
        @yield('content')
      </main>
      <aside class="sidebar col-md-4 col-12 pl-md-4">
        @include('partials.sidebar')
      </aside>
    </div>
  </div>
</div>
@php do_action('get_footer') @endphp
@include('partials.footer')
@php wp_footer() @endphp
</body>
</html>
