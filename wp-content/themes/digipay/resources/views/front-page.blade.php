<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/13/19
 * Time: 11:35 AM
 */
?>

@extends('layouts.app')

@section('content')

  <section id="slider">
    @component('components.slider', [
      'list_slider' => $list_slider
    ])

    @endcomponent
  </section>

  @component('components.popup-success')
  @endcomponent

  <section id="section-loan">
    @include('partials.form-loan', [
      'list_product_item' => $list_product_item,
      'list_district' => $list_district,
    ])
  </section>

  <section id="section-about">
    @include('partials.home.about', [
      'list_about_item' => $list_about_item,
    ])
  </section>

  <section id="section-news">
    @include('partials.home.news', [
      'lasted_posts' => $lasted_posts,
    ])
  </section>

  <section id="section-partner">
    @include('partials.home.partner', [
      'list_partner' => $list_partner,
    ])
  </section>

@endsection
