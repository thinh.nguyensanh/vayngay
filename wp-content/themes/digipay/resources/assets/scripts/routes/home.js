import $ from 'jquery';
import 'slick-carousel';
import 'select2';
import 'jquery-validation';
import * as Ladda from 'ladda';
import SmoothScroll from 'smooth-scroll';

export default {
  init() {

    // JavaScript to be fired on the home page
    $('.home-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
    });

    $('.list-news').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      mobileFirst: true,
      dots: true,
      prevArrow:'<button class="PrevArrow"><i class="fas fa-caret-left"></i></button>',
      nextArrow:'<button class="NextArrow"><i class="fas fa-caret-right"></i></button>',
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 320,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });

    function init_select(){
      // select2
      $('select[name="address"]').select2({
        width: '100%',
        placeholder: 'Chọn/Gõ địa chỉ',
      });

      $('select[name="productCode"]').select2({
        width: '100%',
        placeholder: 'Chọn hình thức vay',
      });

      $('select[name="income"]').select2({
        width: '100%',
        placeholder: 'Chọn mức thu nhập phù hợp',
      });

      $('select[name="loanAmount"]').select2({
        width: '100%',
        placeholder: 'Chọn khoản vay',
      });

      $('select[name="loanTerm"]').select2({
        width: '100%',
        placeholder: 'Chọn thời hạn vay',
      });
    }

    init_select();

    $.validator.addMethod(
      'regex',
      function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
      },
      'Please check your input.'
    );

    let scroll = new SmoothScroll();

    // get data form
    $('#form-prescoring').validate({
      // Specify validation rules
      rules: {
        fullName: 'required',
        phoneNumber: {
          required: true,
          digits: true,
          regex: '^(\\d{10}|\\d{11})$',
        },
        idNumber: {
          required: true,
          digits: true,
          regex: '^(\\d{9}|\\d{12})$',
        },
        address: {
          required: true,
        },
      },
      // Specify validation error messages
      messages: {
        fullName: 'Bạn chưa nhập họ tên.',
        phoneNumber: {
          required: 'Bạn chưa nhập số điện thoại',
          digits: 'Số điện thoại chỉ được nhập số',
          regex: 'Chỉ bao gồm 10 hoặc 11 số',
        },
        idNumber: {
          required: 'Bạn chưa nhập số chứng minh',
          digits: 'Số chứng minh chỉ được nhập số',
          regex: 'Chỉ bao gồm 9 hoặc 12 số',
        },
        address: 'Bạn chưa nhập địa chỉ',
      },
      errorPlacement: function(error, element) {
        if (element.attr('name') === 'address')
          error.insertAfter('#group-address .select2-container');
        else
          error.insertAfter(element);
      },

      // in the "action" attribute of the form when valid
      submitHandler: function() {
        var l = Ladda.create( document.querySelector( '#btn-loan-reg' ) );
        var formData = $('#form-prescoring').serialize();
        const ajax_url = '/wp-admin/admin-ajax.php';

        $.ajax({
          url: ajax_url,
          method: 'POST',
          dataType: 'json',
          data: {
            action: 'check_prescoring',
            formData: formData,
          },
          beforeSend: function() {
            l.start();
          },
          success: function(data) {
            l.stop();
            let notice_title = $('.notice--check-loan__title');
            let notice_content = $('.notice--check-loan__content');
            let form_final = $('#form-final');
            var anchor = document.querySelector('#btn-loan-reg');
            notice_title.removeClass('success');
            notice_title.removeClass('failed');

            if (data.status){
              notice_title.html('<p>Đủ điều kiện</p>');
              notice_title.addClass('success');
              notice_content.html('<p>Chúc mừng bạn đã đủ điều kiện vay bước 1! <br/> Vui lòng nhập đầy đủ các thông tin còn lại để được tư vấn miễn phí!</p>');
              form_final.css('display','block');
              scroll.animateScroll(anchor, '', { speed: 1500 });
            } else {
              notice_title.html('<p>Không đủ điều kiện</p>');
              notice_title.addClass('failed');
              notice_content.html('<p>Rất tiếc, bạn không đủ điều kiện vay tại Vayngay. <br/> Vui lòng quay lại vào thời gian khác!!!</p>');
            }
          },
          error: function() {
            alert('Hệ thống đang bảo trì');
            l.stop();
          },
        })
      },
    });

    $('#form-final').validate({
      submitHandler: function() {
        var l = Ladda.create( document.querySelector( '#btn-loan-final' ) );
        var formData = $('#form-final').serialize();
        const ajax_url = '/wp-admin/admin-ajax.php';

        $.ajax({
          url: ajax_url,
          method: 'POST',
          dataType: 'json',
          data: {
            action: 'update_app_info',
            formData: formData,
          },
          beforeSend: function() {
            l.start();
          },
          success: function(data) {
            l.stop();
            if (data.status){
              reset_all();
              $('#success-modal').modal('show');
            }
          },
          error: function() {
            alert('Hệ thống đang bảo trì');
            l.stop();
          },
        })
      },
    });

    function reset_all(){
      $('#form-final').css('display', 'none');
      document.getElementById('form-final').reset();
      document.getElementById('form-prescoring').reset();
      init_select();
      $('.notice--check-loan__title').text('');
      $('.notice--check-loan__content').text('');

      let product_item = $('.product-item');
      let first_product_item = product_item.first();
      let id_first_product_item = first_product_item.attr('data-id');

      product_item.removeClass('active');
      first_product_item.addClass('active');
      $('input[name="project"]').val(id_first_product_item);

      let anchor = document.querySelector('.list-product');
      scroll.animateScroll(anchor, '', { speed: 1500 });
    }


  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS

  },
};
