import $ from 'jquery';
import SmoothScroll from 'smooth-scroll';

export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    // window.onscroll = function () {
    //   stickyMenu()
    // };
    //
    // var navbar = document.getElementById('navbar');
    // var sticky = navbar.offsetTop - 20;
    // var webContainer = document.getElementsByClassName('web-container');
    //
    // function stickyMenu() {
    //   if (window.pageYOffset >= sticky) {
    //     navbar.classList.add('sticky')
    //     webContainer[0].classList.add('pt-5');
    //   } else {
    //     navbar.classList.remove('sticky');
    //     webContainer[0].classList.remove('pt-5');
    //   }
    // }

    var productItem = $('.product-item');
    productItem.click(function (){
      productItem.removeClass('active');
      $(this).addClass('active');
      let id = $(this).attr('data-id');
      $('input[name="project"]').val(id);
    });

    var scroll = new SmoothScroll();
    var options = { speed: 500, easing: 'easeOutCubic' };
    var btnMenu = $('.menu-item a');

    btnMenu.click(function (){
      // e.preventDefault();
      var dataHref = $(this).attr('href');

      if (/^#/.test(dataHref)) {
        var anchor = document.querySelector(dataHref);

        if (anchor) {
          scroll.animateScroll(anchor, '', options);
        } else {
          window.location.href = '/' + dataHref;
        }
      }
    });
  },
};
