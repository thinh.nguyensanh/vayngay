<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/20/19
 * Time: 5:05 PM
 */

namespace App;

use App\Controllers\FrontPage;

class Ajax
{
    public function __construct()
    {
        add_action('wp_ajax_check_prescoring', [$this, 'check_prescoring']);
        add_action('wp_ajax_nopriv_check_prescoring', [$this, 'check_prescoring']);

        add_action('wp_ajax_update_app_info', [$this, 'update_app_info']);
        add_action('wp_ajax_nopriv_update_app_info', [$this, 'update_app_info']);
    }

    public function check_prescoring()
    {
        $form_data = [];
        parse_str($_POST['formData'], $form_data);

        foreach ($form_data as $key => $data) {
            $form_data[$key] = sanitize_text_field( $data );
        }

        $response = [
            'status' => false,
        ];

        if (!empty($form_data)){
            $idNumber = $form_data['idNumber'];
            if ($idNumber){
                $response_prescoring = FrontPage::check_prescoring($idNumber);

                if ($response_prescoring){

                    $result_import_lead = FrontPage::import_lead($form_data);
                    if ($result_import_lead['status']){

                        session_start();
                        $_SESSION["appID"] = $result_import_lead['appID'];

                        $response = [
                            'status' => true,
                        ];
                    }

                }
            }
        }

        echo json_encode($response);

        wp_die();
    }

    public function update_app_info() {
        $form_data = [];
        parse_str($_POST['formData'], $form_data);

//        foreach ($form_data as $key => $data) {
//            $form_data[$key] = sanitize_text_field( $data );
//        }

        session_start();
        if ($_SESSION["appID"]){
            $form_data['appID'] = $_SESSION["appID"];
        }

        $result_update_app_info = FrontPage::update_app_info($form_data);

        $response = [
            'status' => true,
        ];

        session_unset();
        session_destroy();

        echo json_encode($response);

        wp_die();
    }
}

new Ajax();
