<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public static $api_url = 'https://appay.cloudcms.vn/api/import/';
    public static $token = 'a6fcddbb3bd4e64c37819f839adb3dce';

    public function __before()
    {

    }

    public function list_slider(){
        $field_slider = get_field('slider');
        $list_slider = [];

        foreach ($field_slider as $slider){
            $list_slider[] = [
                'image' => $slider['slider_image'],
            ];
        }

        return $list_slider;
    }

    public function list_product_item(){
        $list_product_item = [
            [
            'id' => '1',
            'title' => 'Vay tiền mặt',
            'image' => '/wp-content/themes/digipay/resources/assets/images/home/cash-1.svg',
            ],
            [
            'id'=> '5',
            'title' => 'Vay mua hàng hóa trả góp',
            'image' => '/wp-content/themes/digipay/resources/assets/images/home/web.svg',
            ],
//            [
//            'id'=> '11',
//            'title' => 'Vay mua xe trả góp',
//            'image' => '/wp-content/themes/digipay/resources/assets/images/home/scooter.svg',
//            ],
            [
            'id'=> '1',
            'title' => 'Thẻ tín dụng',
            'image' => '/wp-content/themes/digipay/resources/assets/images/home/credit-card.svg',
            ],
        ];

        return $list_product_item;
    }

    public function list_about_item(){
        $list_about_item = [
            [
                'title' => 'Thương hiệu uy tín',
                'desc' => 'Thuộc quyền sở hữu công ty Cổ Phần Digipay, Vayngay hiện đang là đối tác chiến lược của hầu hết các công ty tài chính và ngân hàng cho vay tín chấp uy tín tại Việt Nam.',
                'image' => '/wp-content/themes/digipay/resources/assets/images/home/icon-high-five.svg',
                'class' => ''
            ],
            [
                'title' => 'Xử lý nhanh chóng',
                'desc' => 'Đội ngũ nhân viên gồm 200 nhân viên tư vấn qua điện thoại, 5000 nhân viên bán hàng trực tiếp phủ sóng toàn quốc luôn sẵn sàng phục vụ quý khách hàng nhanh nhất có thể.',
                'image' => '/wp-content/themes/digipay/resources/assets/images/home/icon-clock.svg',
                'class' => 'border-bottom-dark'
            ],
            [
                'title' => 'Lãi suất ưu đãi',
                'desc' => 'Đến với Vayngay, khách hàng được tư vấn miễn phí sản phẩm với lãi suất ưu đãi nhất, chỉ từ 1,32%/ tháng và giấy tờ sao cho đơn giản nhất. ',
                'image' => '/wp-content/themes/digipay/resources/assets/images/home/icon-light-bulb.svg',
                'class' => 'border-bottom-dark'
            ],
            [
                'title' => 'Minh bạch, tin cậy',
                'desc' => '100% sản phẩm Vayngay đang phục vụ đều đến từ các tổ chức tài chính và ngân hàng uy tín được Nhà Nước cấp phép kinh doanh.',
                'image' => '/wp-content/themes/digipay/resources/assets/images/home/icon-shield.svg',
                'class' => ''
            ],
        ];

        return $list_about_item;
    }

    public function lasted_posts(){
        $args = [
            'post_type' => 'post',
            'post_status' => 'publish',
            'orderby' => 'date',
            'showposts' => 5,
        ];

        $list_post = get_posts( $args );

        $return_arr = array_map( function ($post){
            $featured_image_id = get_post_thumbnail_id($post->ID);
            $category_detail = get_the_category($post->ID);
            return [
                'ID' => $post->ID,
                'name' => $post->post_title,
                'url' => $post->guid,
                'image' => $featured_image_id ? wp_get_attachment_url($featured_image_id) : '',
                'views' => get_field('views', $post->ID),
                'category_name' => !empty($category_detail) ? $category_detail[0]->name : '',
                'post_date' => date("d/m/Y", strtotime($post->post_date)),
            ];
        }, $list_post);

        return $return_arr;
    }

    public function list_partner(){
        $arr_logo = [
            [
                'image' => '/wp-content/themes/digipay/resources/assets/images/home/logo-fe-credit.svg',
            ],
            [
                'image' => '/wp-content/themes/digipay/resources/assets/images/home/logo-mcredit.svg',
            ],
            [
                'image' => '/wp-content/themes/digipay/resources/assets/images/home/logo-easy-credit.svg',
            ],
//            [
//                'image' => '/wp-content/themes/digipay/resources/assets/images/home/logo-shb.svg',
//            ],
        ];

        return $arr_logo;
    }

    public function list_product() {
        $list_product = [
            [
                'code' => '9',
                'name' => 'Theo sim Viettel/ Mobifone',
            ],
            [
                'code' => '1',
                'name' => 'Theo bảng lương',
            ],
            [
                'code' => '3',
                'name' => 'Theo cà vẹt xe',
            ],
            [
                'code' => '2',
                'name' => 'Theo hóa đơn điện',
            ],
            [
                'code' => '14',
                'name' => 'Theo số dư thẻ ATM',
            ],
            [
                'code' => '6',
                'name' => 'Theo hợp đồng bảo hiểm nhân thọ',
            ],
            [
                'code' => '4',
                'name' => 'Theo hợp đồng mua hàng trả góp tại tổ chức khác',
            ],
            [
                'code' => '17',
                'name' => 'Chưa xác định',
            ],
        ];

        return $list_product;
    }

    public function list_income() {
        $list_income = [
            [
                'code' => '<5000000',
                'name' => 'Dưới 5.000.000 vnđ',
            ],
            [
                'code' => '5000000-8000000',
                'name' => 'Từ 5.000.000 vnđ - 8.000.000 vnđ',
            ],
            [
                'code' => '8000000-12000000',
                'name' => 'Từ 8.000.000 vnđ - 12.000.000 vnđ',
            ],
            [
                'code' => '>12000000',
                'name' => 'Trên 12.000.000 vnđ',
            ],
            [
                'code' => 'unknown',
                'name' => 'Chưa xác định',
            ],
        ];

        return $list_income;
    }

    public function list_loan_amount() {
        $list_loan_amount = [
            [
                'code' => '<10000000',
                'name' => 'Dưới 10.000.000 vnđ',
            ],
            [
                'code' => '10000000-25000000',
                'name' => 'Từ 10.000.000 vnđ - 25.000.000 vnđ',
            ],
            [
                'code' => '25000000-50000000',
                'name' => 'Từ 25.000.000 vnđ - 50.000.000 vnđ',
            ],
            [
                'code' => '50000000-70000000',
                'name' => 'Từ 50.000.000 vnđ - 70.000.000 vnđ',
            ],
            [
                'code' => '>70000000',
                'name' => 'Trên 70.000.000 vnđ',
            ],
        ];

        return $list_loan_amount;
    }

    public function list_loan_term() {
        $list_loan_term = [
            [
                'code' => '<12',
                'name' => 'Dưới 12 tháng',
            ],
            [
                'code' => '12-24',
                'name' => 'Từ 12 - 24 tháng',
            ],
            [
                'code' => '24-36',
                'name' => 'Từ 24 - 36 tháng',
            ],
            [
                'code' => '>36',
                'name' => 'Trên 36 tháng',
            ],
        ];

        return $list_loan_term;
    }

    public function list_district() {
        $link_api = FrontPage::$api_url.'get_district_code';

        $args = [
            'token' => FrontPage::$token,
        ];

        $param_string = http_build_query($args);

        $result = wp_remote_get($link_api.'?'.$param_string);

        try {
            if (!isset($result->errors)){
                $result_decode = json_decode( $result['body'], true );
            }
        } catch ( Exception $ex ) {
            $result_decode = [];
        } // end try/catch

        if (!empty($result_decode)){
            $status = $result_decode['status'];
            if ($status) {
                return $result_decode['data'];
            }
        }
    }

    public static function check_prescoring($idNumber) {
        $link_api = FrontPage::$api_url.'prescoring';

        $args = [
            'idNumber' => $idNumber,
            'token' => FrontPage::$token,
        ];

        $param_string = http_build_query($args);

        $result = wp_remote_get($link_api.'?'.$param_string);

        try {
            if (!isset($result->errors)){
                $result_decode = json_decode( $result['body'], true );
            }
        } catch ( Exception $ex ) {
            $result_decode = [];
        } // end try/catch

        if (!empty($result_decode)){
            $status = $result_decode['status'];
            if ($status){
                return true;
            } else {
                return false;
            }
        }
    }

    public static function import_lead($form_data) {
        $link_api = FrontPage::$api_url.'lead';

        $args = [
            'body' => [
                'name' => $form_data['fullName'],
                'idNumber' => $form_data['idNumber'],
                'mobilePhone' => $form_data['phoneNumber'],
                'district' => $form_data['address'],
                'preDSA_idNumber' => '021288132',
                'token' => FrontPage::$token,
                'projectID' => $form_data['project'],
            ],
        ];

        if (isset($_COOKIE['utm_source'])) {
            $args['body']['note'] = 'Khách hàng từ vayngay '.$_COOKIE['utm_source'];
        } else {
            $args['body']['note'] = 'Khách hàng từ vayngay';
        }

        $result = wp_remote_post($link_api, $args);

        try {
            if (!isset($result->errors)) {
                $result_decode = json_decode($result['body'], true);
            }
        } catch ( Exception $ex ) {
            $result_decode = [];
        } // end try/catch

        return $result_decode;
    }

    public static function update_app_info($form_data) {
        $link_api = FrontPage::$api_url.'update_lead_info';

        $args = [
            'body' => [
                'appID' => $form_data['appID'],
                'productCode' => $form_data['productCode'],
                'income' => $form_data['income'],
                'loanAmount' => $form_data['loanAmount'],
                'loanTerm' => $form_data['loanTerm'],
                'token' => FrontPage::$token,
            ],
        ];

        if (isset($_COOKIE['utm_source'])) {
            $args['body']['utmSource'] = $_COOKIE['utm_source'];
        }

        $result = wp_remote_post($link_api, $args);

        try {
            if (!isset($result->errors)) {
                $result_decode = json_decode($result['body'], true);
            }
        } catch ( Exception $ex ) {
            $result_decode = [];
        } // end try/catch

        return $result_decode;
    }

}
