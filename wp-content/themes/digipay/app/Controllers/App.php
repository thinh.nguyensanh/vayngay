<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public function popular_posts(){
        $args = [
            'post_type' => 'post',
            'post_status' => 'publish',
            'meta_key' => 'views',
            'orderby' => 'meta_value_num',
            'showposts' => 5,
        ];

        $list_post = get_posts( $args );

        $return_arr = array_map( function ($post){
            $featured_image_id = get_post_thumbnail_id($post->ID);
            $category_detail = get_the_category($post->ID);
            return [
                'ID' => $post->ID,
                'name' => $post->post_title,
                'url' => $post->guid,
                'image' => $featured_image_id ? wp_get_attachment_url($featured_image_id) : '',
                'views' => get_field('views', $post->ID),
                'category_name' => !empty($category_detail) ? $category_detail[0]->name : '',
                'post_date' => date("d/m/Y", strtotime($post->post_date)),
            ];
        }, $list_post);

        return $return_arr;
    }

    public function all_tags() {
        $all_tags = get_tags();
        return $all_tags;
    }

    public function all_categories(){
        $all_categories = get_categories();

        return $all_categories;
    }

    public static function pagination_link($numpages = '', $pagerange = '', $paged='') {

        if (empty($pagerange)) {
            $pagerange = 2;
        }
        global $paged;
        if (empty($paged)) {
            $paged = 1;
        }
        if ($numpages == '') {
            global $wp_query;
            $numpages = $wp_query->max_num_pages;
            if(!$numpages) {
                $numpages = 1;
            }
        }

        $pagination_args = array(
            'base'            => @add_query_arg('paged','%#%'),
            'format'          => '',
            'total'           => $numpages,
            'current'         => $paged,
            'show_all'        => False,
            'end_size'        => 1,
            'mid_size'        => $pagerange,
            'prev_next'       => True,
            'prev_text'       => __('<'),
            'next_text'       => __('>'),
            'type'            => 'array',
            'add_args'        => false,
            'add_fragment'    => ''
        );

        $paginate_links = paginate_links($pagination_args);

        if (is_array($paginate_links)) {
            echo "<div class='cpagination'>";
            echo '<ul class="pagination">';
            foreach ( $paginate_links as $page ) {
                echo "<li>$page</li>";
            }
            echo '</ul>';
            echo "</div>";
        }

    }

    function set_cookie_source() {
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $parts = parse_url($actual_link);

        if (isset($parts['query'])) {
            parse_str($parts['query'], $list_param);
            if (isset($list_param['utm_source'])) {
                $cookie_name = "utm_source";
                $cookie_value = $list_param['utm_source'];
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
            }
        }

//        if (isset($_COOKIE['utm_source'])) {
//            echo $_COOKIE['utm_source'];
//        }
    }

}
