<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/16/19
 * Time: 10:58 AM
 */

namespace App\Controllers;

use Sober\Controller\Controller;

class SinglePost extends Controller
{
    public function __after()
    {
        // runs after all the class methods have run
        $post_id = get_the_ID();
        $post_view = get_field('views');
        $post_view = (int) $post_view + 1;
        update_post_meta($post_id, 'views', $post_view);
    }

    public function related_posts() {
        $post_id = get_the_ID();
        $post_category = get_the_category($post_id);

        foreach ($post_category as $category) $arr_post_category[] = $category->term_id;

        $args = [
            'post_type' => 'post',
            'post_status' => 'publish',
            'orderby' => 'date',
            'showposts' => 2,
            'post__not_in' => [$post_id],
        ];

        if (!empty($arr_post_category)) $args['category__in'] = $arr_post_category;

        $list_post = get_posts( $args );

        $return_arr = array_map( function ($post){
            $featured_image_id = get_post_thumbnail_id($post->ID);
            $category_detail = get_the_category($post->ID);
            return [
                'ID' => $post->ID,
                'name' => $post->post_title,
                'excerpt' => wp_trim_words( $post->post_content, 40, '...' ),
                'url' => $post->guid,
                'image' => $featured_image_id ? wp_get_attachment_url($featured_image_id) : '',
                'views' => get_field('views', $post->ID),
                'category_name' => !empty($category_detail) ? $category_detail[0]->name : '',
                'post_date' => date("d/m/Y", strtotime($post->post_date)),
            ];
        }, $list_post);

        return $return_arr;
    }

}
