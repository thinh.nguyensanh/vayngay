if ($('.related-post-carousel').length) {
    $('.related-post-carousel').owlCarousel({
        loop:true,
        margin: 30,
        dots: false,
        nav: false,
        stagePadding: 0,
        singleItem:true,
        smartSpeed: 500,
        autoplay: true,
        autoplayTimeout:2000,
        navText: [ '<span class="fa fa-long-arrow-left"></span><p>Prev</p>', '<span class="fa fa-long-arrow-right right"></span><p>Next</p>' ],
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1024:{
                items:3
            }
        }
    });    		
}
if ($('.client-carousel').length) {
    $('.client-carousel').owlCarousel({
        loop:false,
        margin: 30,
        dots: false,
        nav: false,
        stagePadding: 0,
        singleItem:true,
        smartSpeed: 500,
        autoplay: false,
        autoplayTimeout:3000,
        navText: [ '<span class="fa fa-long-arrow-left"></span><p>Prev</p>', '<span class="fa fa-long-arrow-right right"></span><p>Next</p>' ],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1024:{
                items:5
            }
        }
    });    		
}
