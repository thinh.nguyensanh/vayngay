<?php

$context            =   Timber::context();
$post               =   new Timber\Post();
$context['post']    =   $post;

Timber::render( 'single/page.twig', $context );

?>