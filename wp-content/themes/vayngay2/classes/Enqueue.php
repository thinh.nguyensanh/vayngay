<?php
namespace Config;
class Enqueue
{
    public function __construct(){
        add_action( 'wp_enqueue_scripts', array( $this, 'guest_styles' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'guest_scripts' ) );
    }
    public function guest_styles(){
        wp_enqueue_style( 'vayngay-bootstrap', THEME_DIR . '/public/css/bootstrap.css', array(), THEME_VERSION );
        wp_enqueue_style( 'vayngay-style', THEME_DIR . '/public/css/style.css', array(), THEME_VERSION );

        // Banner
        // wp_enqueue_style( 'vayngay-normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css', array(), THEME_VERSION );
        // wp_enqueue_style( 'vayngay-animateslide', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css', array(), THEME_VERSION );
        wp_enqueue_style( 'vayngay-slide', THEME_DIR . '/public/css/slide.css', array(), THEME_VERSION );


        wp_enqueue_style( 'vayngay-fancybox', 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css', array(), THEME_VERSION );

        wp_enqueue_style( 'vayngay-select2', THEME_DIR . '/public/css/select2.min.css', array(), THEME_VERSION );
        wp_enqueue_style( 'vayngay-owl', THEME_DIR . '/public/css/owl.css', array(), THEME_VERSION );
        // wp_enqueue_style( 'vayngay-owl-default', THEME_DIR . '/public/css/owl.theme.default.css', array(), THEME_VERSION );

        wp_enqueue_style( 'vayngay-responsive', THEME_DIR . '/public/css/responsive.css', array(), THEME_VERSION );
        wp_enqueue_style( 'vayngay-color1', THEME_DIR . '/public/css/colors/color1.css', array(), THEME_VERSION );
        wp_enqueue_style( 'vayngay-animate', THEME_DIR . '/public/css/animate.css', array(), THEME_VERSION );
        wp_enqueue_style( 'vayngay-custom', THEME_DIR . '/public/css/custom.css', array(), THEME_VERSION );
        wp_enqueue_style( 'vayngay-main', THEME_DIR . '/public/css/main.css', array(), THEME_VERSION );

        wp_enqueue_style( 'vayngay-content', THEME_DIR . '/public/css/content.css', array(), THEME_VERSION );
        wp_enqueue_style( 'vayngay-form', THEME_DIR . '/public/css/form.css', array(), THEME_VERSION );
    }
    
    public function guest_scripts(){
        // wp_enqueue_script( 'vayngay-jquery', THEME_DIR . '/public/javascript/jquery.min.js', array(), THEME_VERSION, true );
        wp_enqueue_script( 'vayngay-jquery',  'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', array(), THEME_VERSION, true );
        wp_enqueue_script( 'vayngay-select2',  'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array(), THEME_VERSION, true );
        wp_enqueue_script( 'vayngay-fancybox',  'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js', array(), THEME_VERSION, true );
        wp_enqueue_script( 'vayngay-owl.carousel.min',  THEME_DIR . '/public/js/owl.carousel.min.js', array(), THEME_VERSION, true );
        // wp_enqueue_script( 'vayngay-bootstrap', THEME_DIR . '/public/javascript/bootstrap.min.js', array(), THEME_VERSION, true );

        // wp_enqueue_script( 'vayngay-imagesloaded.min', THEME_DIR . '/public/javascript/imagesloaded.min.js', array(), THEME_VERSION, true );
        // wp_enqueue_script( 'vayngay-jquery-waypoints', THEME_DIR . '/public/javascript/jquery-waypoints.js', array(), THEME_VERSION, true );

        // wp_enqueue_script( 'vayngay-jquery-countTo', THEME_DIR . '/public/javascript/jquery-countTo.js', array(), THEME_VERSION, true );
        // wp_enqueue_script( 'vayngay-carousel', THEME_DIR . '/public/javascript/owl.carousel.js', array(), THEME_VERSION, true );

        // wp_enqueue_script( 'vayngay-bxslider', THEME_DIR . '/public/javascript/jquery.bxslider.js', array(), THEME_VERSION, true );

        wp_enqueue_script( 'vayngay-main', THEME_DIR . '/public/javascript/main.js', array(), THEME_VERSION, true );

        
    }
}