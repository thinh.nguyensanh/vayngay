<?php
$editor = get_field('default_editor', 'option') ?: 'classic';
if($editor==='classic'){
	add_filter('use_block_editor_for_post', '__return_false');
}

add_action('admin_init', 'remove_danger_options_page', 99);
function remove_danger_options_page() {
   remove_menu_page('acf-options');
   remove_submenu_page('themes.php', 'theme-editor.php');
}

add_action('admin_init', 'remove_menu_page_if_not_admin', 199);
function remove_menu_page_if_not_admin() {
   if(!current_user_can('administrator')){
      // remove_menu_page('wpcf7');
      // remove_menu_page('wp-mail-smtp');
      remove_menu_page('tools.php');
   }
}

add_filter('acf/settings/show_admin', 'my_acf_show_admin');
function my_acf_show_admin($show) {
   return false;
}

add_filter('mime_types', 'webp_upload_mimes');
function webp_upload_mimes($mime_types) {
   $mime_types['webp'] = 'image/webp';
   $mime_types['svg'] = 'image/svg+xml';
   return $mime_types;
}

add_action( 'phpmailer_init', function( $phpmailer ) {
    $phpmailer->isSMTP();
    $phpmailer->Host       = 'smtp.gmail.com';
    $phpmailer->Port       = '587';
    $phpmailer->SMTPSecure = 'tls';
    $phpmailer->SMTPAuth   = true;
    $phpmailer->Username   = get_option('options_smtp_email');
    $phpmailer->Password   = get_option('options_smtp_pass');
});
add_filter('wp_mail_from', function(){
   return get_option('options_smtp_email');
});
add_filter( 'wp_mail_from_name', function( $name ) {
   return get_option('options_smtp_name');
} );
add_action('wp_mail_failed', function( $wp_error ){
   $fn = ABSPATH . '/mail.log';
   $fp = fopen($fn, 'a');
   fputs($fp, "Mail Error: " . $wp_error->get_error_message() ."\n");
   fclose($fp);
}, 10, 1);