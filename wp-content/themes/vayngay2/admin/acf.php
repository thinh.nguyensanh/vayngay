<?php
$acf = [
	'general',
    'sidebar',
    'home',
    'partner',
    'product',
    'form',
    'banner',
    'intro'
];
require_files('/admin/acf', $acf);

if( function_exists('acf_add_options_page') ) {

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Cài đặt chung',
        'menu_title'	=> 'Cài đặt chung',
        'parent_slug'	=> 'themes.php',
        'capability' 	=> 'administrator',
        'update_button' 	=> __('Cập nhật', TEXTDOMAIN),
        'updated_message' 	=> __('Cài đặt đã được cập nhật.', TEXTDOMAIN)
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Cài đặt sidebar',
        'menu_title'	=> 'Cài đặt sidebar',
        'parent_slug'	=> 'themes.php',
        'capability' 	=> 'administrator',
        'update_button' 	=> __('Cập nhật', TEXTDOMAIN),
        'updated_message' 	=> __('Cài đặt đã được cập nhật.', TEXTDOMAIN)
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Cài đặt form',
        'menu_title'	=> 'Cài đặt form',
        'parent_slug'	=> 'edit.php?post_type=loan',
        'capability' 	=> 'administrator',
        'update_button' 	=> __('Cập nhật', TEXTDOMAIN),
        'updated_message' 	=> __('Cài đặt đã được cập nhật.', TEXTDOMAIN)
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Cài đặt sản phẩm',
        'menu_title'	=> 'Cài đặt sản phẩm',
        'parent_slug'	=> 'edit.php?post_type=product',
        'capability' 	=> 'administrator',
        'update_button' 	=> __('Cập nhật', TEXTDOMAIN),
        'updated_message' 	=> __('Cài đặt đã được cập nhật.', TEXTDOMAIN)
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Cài đặt bài viết',
        'menu_title'	=> 'Cài đặt bài viết',
        'parent_slug'	=> 'edit.php',
        'capability' 	=> 'administrator',
        'update_button' 	=> __('Cập nhật', TEXTDOMAIN),
        'updated_message' 	=> __('Cài đặt đã được cập nhật.', TEXTDOMAIN)
    ));
}