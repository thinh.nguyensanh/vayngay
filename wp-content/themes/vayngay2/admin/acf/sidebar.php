<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_60e27697ad796',
	'title' => 'Cài đặt sidebar',
	'fields' => array(
		array(
			'key' => 'field_60e7d3d4c6777',
			'label' => 'Sidebar sản phẩm',
			'name' => 'sidebar_product',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'layouts' => array(
				'layout_60e277cff5b7d' => array(
					'key' => 'layout_60e277cff5b7d',
					'name' => 'product_widget',
					'label' => 'Sản phẩm',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_60e7d3d4c677c',
							'label' => 'Chọn sản phẩm',
							'name' => 'list',
							'type' => 'post_object',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'post_type' => array(
								0 => 'product',
							),
							'taxonomy' => '',
							'allow_null' => 0,
							'multiple' => 1,
							'return_format' => 'object',
							'ui' => 1,
						),
					),
					'min' => '',
					'max' => '',
				),
			),
			'button_label' => 'Thêm Block',
			'min' => '',
			'max' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-cai-dat-sidebar',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;

?>