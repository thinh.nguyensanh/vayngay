<?php

$timberContext = $GLOBALS['timberContext'];
if ( ! isset( $timberContext ) ) {
	throw new \Exception( 'Chưa gọi context.' );
}
$timberContext['content'] = ob_get_contents();
ob_end_clean();

Timber::render( 'page-plugin.twig', $timberContext );

?>