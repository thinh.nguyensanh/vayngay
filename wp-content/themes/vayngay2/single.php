<?php
/** File này xử lý dữ liệu cho các trang chi tiết bài viết (post hoặc custom post type) */
$context  			= 	Timber::context();
$post				= 	Timber::get_post();
$context['post'] 	= 	$post;
$context['title'] 	= 	$post->title;

if( post_password_required( $post->ID ) ) {
	Timber::render( 'single/password.twig', $context );
}else {
	$post_type = get_post_type();
	switch($post_type) {
		case 'post':
			$template = array( 'single/post.twig' );
			$taxonomy = 'category';
			if($post->terms){
				$post_terms = [];
				foreach($post->terms as $term){
					$post_terms[] = $term->term_id;
				}
				$related_posts =  new Timber\PostQuery(array(
					'post_type'		=>	$post_type,
					'numberposts' 	=>  6, 
					'post__not_in' 	=>  array($post->ID),
					'post_status'	=> 'publish',
					'tax_query' 	=> array(
						array(
							'taxonomy'      =>  $taxonomy,
							'field'         =>  'term_id',
							'terms'         =>  $post_terms,
						)
					)
				));
				$context['related_posts'] = $related_posts;
			}
			break;
		case 'product':
			$template = array( 'single/product.twig' );
			$taxonomy = 'category';
			break;
		default :
			break;	
	}
	Timber::render( $template, $context );
}

?>