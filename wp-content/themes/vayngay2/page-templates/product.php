<?php 
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$context            =   Timber::context();
$post               =   new Timber\Post();
$context['post']    =   $post;
$template = array( 'archive/product.twig' );
$context['posts'] = new Timber\PostQuery([
    'post_type'     => 'product',
    'post_status'   => 'publish',
    'paged'         =>  $paged
]);
Timber::render($template, $context);