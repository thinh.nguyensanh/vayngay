<?php 
/** Template Name: Trang chủ
*/

$context            =   Timber::context();
$post               =   new Timber\Post();
$context['post']    =   $post;
Timber::render( 'page-templates/homepage.twig', $context );
