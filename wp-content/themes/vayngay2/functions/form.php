<?php

function submit_loan_action(){
    $factory = new Config\Validator();

    $after_date = date('Y-m-d',strtotime('-60 year', strtotime(current_time("Y-m-d"))));
    $before_date = date('Y-m-d',strtotime('-18 year', strtotime(current_time("Y-m-d"))));

    $messages = [
        'loan_exhibit.required'         => 'Cần chọn ít nhất 1 loại giấy tờ.',

        'loan_money.min'                => 'Hãy chọn số tiền cần vay.',
        'loan_money.max'                => 'Số tiền tối đa có thể vay là 100.000.000 đồng.',

        'loan_time.min'                 => 'Thời gian vay tối thiểu là 6 tháng.',
        'loan_time.max'                 => 'Thời gian vay tối đa là 36 tháng.',

        'loan_name.required'            => 'Họ tên không được để trống.',

        'loan_identify.required'        => 'CMND không được để trống.',
        'loan_identify.min'             => 'CMND không hợp lệ.',
        'loan_identify.max'             => 'CMND không hợp lệ.',

        'loan_email.required'           => 'Email không được để trống.',
        'loan_email.email'              => 'Email không hợp lệ.',

        'loan_phone.required'           => 'Số điện thoại không được để trống.',
        'loan_phone.numeric'            => 'Số điện thoại không hợp lệ.',
        'loan_phone.regex'              => 'Số điện thoại không hợp lệ.',

        'loan_date.required'            => 'Ngày sinh không được để trống.',
        'loan_date.date_format'         => 'Ngày sinh không hợp lệ.',
        'loan_date.after'               => 'Người vay không được quá 60 tuổi.',
        'loan_date.before'              => 'Người vay phải từ 18 tuổi trở lên.',

        'loan_province.required'        => 'Tỉnh thành không được để trống.',

        'g-recaptcha-response.required' =>  'Vui lòng xác nhận không phải là người máy.'
    ];

    $rules = [
        'loan_exhibit'          => 'required',
        'loan_money'            => 'numeric|min:5000000|max:100000000',
        'loan_time'             => 'numeric|min:6|max:36',
        'loan_name'             => 'required',
        'loan_identify'         => 'required|min:9|max:12',
        'loan_email'            => 'required|email:rfc,dns',
        'loan_phone'            => 'required|numeric|regex:/^\d{9,12}$/',
        'loan_date'             => "required|date_format:Y-m-d|before:$before_date|after:$after_date",
        'loan_province'         => 'required',
        'g-recaptcha-response'  => ['required', new GoogleCapcha()]
    ];

    $validator = $factory->make($_POST, $rules, $messages);

    if($validator->fails()){
        $errors = $validator->errors();
        $data = [
            'status'    =>  'error',
            'message'   =>  'Lỗi xác thực thông tin, vui lòng kiểm tra lại!',
            'data'      =>  $errors->getMessages()
        ];
    }else{
        $loan_exhibit = $_POST['loan_exhibit'];
        $loan_money = $_POST['loan_money'];
        $loan_money = number_format($loan_money, 0, '.', ',');
        $loan_time = $_POST['loan_time'];
        $loan_name = $_POST['loan_name'];
        $loan_identify = $_POST['loan_identify'];
        $loan_email = $_POST['loan_email'];
        $loan_phone = $_POST['loan_phone'];
        $loan_date = $_POST['loan_date'];
        $loan_date = date('d-m-Y', strtotime($loan_date));
        $loan_province = $_POST['loan_province'];
        $headers = array(
            'Content-Type: text/html; charset=UTF-8',
            'Reply-to: '.$loan_name.' <'.$loan_email.'>'
        );
        $mail_to = [];
        foreach(get_field('receive_email', 'option') as $value){
            $mail_to[] = $value['email'];
        }
        $mail_title = $loan_name.' - Đăng ký vay';
        $mail_content = "
            <body style='margin: 0; padding: 0;'>
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td style='padding: 0;'>
                            <p>Họ tên: <b>$loan_name</b></p>
                            <p>Ngày sinh: <b>$loan_date</b></p>
                            <p>CMND: <b>$loan_identify</b></p>
                            <p>Tỉnh thành: <b>$loan_province</b></p>
                            <p>SĐT: <b><a href='tel:$loan_phone'>$loan_phone</a></b></p>
                            <p>Email: <b><a href='mailto:$loan_email'>$loan_email</a></b></p>
                            <p>Giấy tờ: <b>$loan_exhibit</b></p>
                            <p>Số tiền vay: <b>$loan_money đồng</b></p>
                            <p>Thời gian vay: <b>$loan_time tháng</b></p>
                        </td>
                    </tr>
                    <tr>
                        <td align='center' style='padding: 20px 0;'>
                            <p>Đây là thông tin được gởi trực tiếp từ khách hàng thông qua website vayngay.vn</p>
                        </td>
                    </tr>
                </table>
            </body>
        ";
        $post_data = array(
            'post_title'    =>  wp_strip_all_tags( $_POST['loan_name'] ),
            'post_content'  =>  $mail_content,
            'post_status'   =>  'publish',
            'post_type'     =>  'loan',
            'post_author'   =>  0
        );
        $post_id = wp_insert_post( $post_data );
        if(!is_wp_error($post_id)){
            wp_mail( $mail_to, $mail_title, $mail_content, $headers );
            $data = [
                'status'    =>  'success',
                'message'   =>  'Đăng ký vay thành công. Xin cảm ơn!',
                'data'      =>  []
            ];
        }else{
            $data = [
                'status'    =>  'failed',
                'message'   =>  'Đã xảy ra lỗi, xin vui lòng thử lại!',
                'data'      =>  []
            ];
        }
    }
    print_r(json_encode($data));
    die;
}
add_action('wp_ajax_submit_loan_action', 'submit_loan_action');
add_action('wp_ajax_nopriv_submit_loan_action', 'submit_loan_action');