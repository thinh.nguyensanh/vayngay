<?php

$context          = Timber::context();
$context['posts'] = new Timber\PostQuery();
$templates        = array( 'index.twig' );
if ( is_home() ) {
	$templates    = array( 'index.twig' );
}
Timber::render( $templates, $context );

?>