$(document).ready(function () {

  function showMessage(message) {
    $('#loan_hande').fancybox({
      'autoScale': true,
      'transitionIn': 'elastic',
      'transitionOut': 'elastic',
      'speedIn': 500,
      'speedOut': 300,
      'autoDimensions': false,
      'centerOnScroll': false
    }).click();
    $('#result-message').html(message);
  }

  function showRule() {
    $('#loan_rule').fancybox({
      'autoScale': true,
      'transitionIn': 'elastic',
      'transitionOut': 'elastic',
      'speedIn': 500,
      'speedOut': 300,
      'autoDimensions': false,
      'centerOnScroll': false
    }).click();
  }

  function showLoading(status) {
    if (status === 'on') {
      $('#loan_accept').prop('disabled', true);
      $('.loan_submit_icon').addClass('fa fa-spinner fa-pulse');
    } else {
      $('#loan_accept').prop('disabled', false);
      $('.loan_submit_icon').removeClass('fa fa-spinner fa-pulse');
    }
  }

  if ($('#loan_form').length) {

    var money = document.querySelector('#range-money');
    var rangeMoney = function () {
      var newValue = money.value;
      var target = document.querySelector('#money-show');
      target.innerHTML = new Intl.NumberFormat().format(newValue);
    }
    money.addEventListener("input", rangeMoney);

    var time = document.querySelector('#range-time');
    var rangeTime = function () {
      var newValue = time.value;
      var target = document.querySelector('#time-show');
      target.innerHTML = newValue;
    }
    time.addEventListener("input", rangeTime);

    $('.single-checkbox').on('change', function (e) {
      if ($('input[type=checkbox]:checked').length > 3) {
        $(this).prop('checked', false);
        showMessage("Chỉ có thể chọn tối đa 3 loại giấy tờ.");
      }
    });

    $('.form-info').change(function () {
      $('.' + $(this).attr('name') + '-error').empty();
    });
    /*$(".select-ident").select2({
      maximumSelectionLength: 3,
      language: {
          maximumSelected: function (e) {
              var t = "Chỉ có thể chọn tối đa " + e.maximum + " loại giấy tờ";
              return t;
          }
      },
      placeholder: "Chọn loại giấy tờ (Tối đa 3 loại)",
      tags : true,
    });*/
    $(".loan-day").select2({
      placeholder: 'Chọn ngày'
    });
    $(".loan-month").select2({
      placeholder: 'Chọn tháng'
    });
    $(".loan-year").select2({
      placeholder: 'Chọn năm'
    });
    $(".loan-province").select2({
      placeholder: 'Quận/Huyện - Tỉnh/Thành Phố'
    });

    $('#loan_submit').click(function () {
      showRule();
    });
    $('#loan_accept').click(function () {
      $('#loan_form').trigger('submit');
    });
    $('#loan_form').on('submit', function (event) {
      event.preventDefault();
      showLoading('on');
      $('.show-error').empty();
      // Gọi API lên Portal
      var formData = new FormData(document.getElementById('loan_form'));
      formData.append('action', "submit_loan_action");
      formData.append('loan_exhibit', formData.getAll('loan_exhibit'));
      formData.append('loan_date', formData.get('loan_year') + '-' + formData.get('loan_month') + '-' + formData.get('loan_day'));
      $.ajax({
        type: "POST",
        url: "https://appay.cloudcms.vn/app_api_v1/vayngay/leads/",
        data: {
          fullName: formData.get('loan_name'),
          idNumber: formData.get('loan_identify'),
          districtID: formData.get('loan_province'),
          mobilePhone: formData.get('loan_phone'),
          dateOfBirth: formData.get('loan_year') + '-' + formData.get('loan_month') + '-' + formData.get('loan_day'),
          address: "",
          gender: "",
          requestAmount: formData.get('loan_money'),
          requestMonth: formData.get('loan_time'),
          note: formData.getAll('loan_exhibit').at(-1),
          email: formData.get('loan_email')
        },
        dataType: "json",
        success: function (res) {
          if (res.status) {
            $.ajax({
              type: $(this).attr('method'),
              url: $(this).attr('action'),
              dataType: 'html',
              contentType: false,
              data: formData,
              cache: false,
              processData: false,
              success: function (response) {
                var object = JSON.parse(response);
                grecaptcha.reset();
                showLoading('off');
                if (object['status'] == 'error') {
                  for (const key in object['data']) {
                    $('.' + key + '-error').html(object['data'][key][0]);
                  }
                  $.fancybox.close(true);
                  showMessage(object['message']);
                } else {
                  $.fancybox.close(true);
                  showMessage(object['message']);
                  document.getElementById("loan_form").reset();
                  $('.input-select').val(null).trigger('change');
                }
              },
              error: function (statusCode) {
                showLoading('off');
                grecaptcha.reset();
                console.log(statusCode.status);
              },
            });
          } else {
            showMessage(res.message);
            showLoading('off');
            grecaptcha.reset();
          }
        },
        error: function () {
          showLoading('off');
          grecaptcha.reset();
        }
      });
    });
  }

  if ($(window).width() >= 1200) {
    $(function () {
      if ($('#sticky-sidebar').length) {
        var el = $('#sticky-sidebar');
        var stickyTop = $('#sticky-sidebar').offset().top;
        var stickyHeight = $('#sticky-sidebar').height();
        $(window).scroll(function () {
          var limit = $('#footer-wrapper').offset().top - stickyHeight - 100;
          var windowTop = $(window).scrollTop();
          if (stickyTop < windowTop) {
            el.css({
              position: 'fixed',
              top: 120
            });
          } else {
            el.css('position', 'static');
          }
          if (limit < windowTop) {
            var diff = limit - windowTop;
            el.css({
              top: diff
            });
          }
        });
      }
    });
  }
  // Back to top button
  (function () {
    $(document).ready(function () {
      return $(window).scroll(function () {
        return $(window).scrollTop() > 600 ? $(".go-top").addClass("show") : $(".go-top").removeClass("show")
      }), $(".go-top").click(function () {
        return $("html,body").animate({
          scrollTop: "0"
        })
      })
    })
  }).call(this);

  if ($('.banner-carousel').length) {
    $('.banner-carousel').owlCarousel({
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      loop: true,
      margin: 0,
      nav: true,
      dots: false,
      smartSpeed: 500,
      height: 700,
      autoHeight: true,
      autoplay: false,
      autoplayTimeout: 5000,
      navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
      responsive: {
        0: {
          items: 1,
          nav: false,
        },
        600: {
          items: 1,
          nav: false,
        },
        1024: {
          items: 1,
          nav: true,
        },
      }
    });
  }
  if ($('.related-carousel').length) {
    $('.related-carousel').owlCarousel({
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      loop: true,
      margin: 0,
      nav: true,
      dots: false,
      margin: 20,
      smartSpeed: 500,
      autoHeight: true,
      autoplay: true,
      autoplayTimeout: 3000,
      navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
      responsive: {
        0: {
          items: 1,
          nav: false,
        },
        600: {
          items: 2,
          nav: false,
        },
        1024: {
          items: 2,
          nav: true,
        },
      }
    });
  }
  if ($('.news-carousel').length) {
    $('.news-carousel').owlCarousel({
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      loop: true,
      margin: 0,
      nav: true,
      dots: false,
      margin: 20,
      smartSpeed: 500,
      autoHeight: true,
      autoplay: true,
      autoplayTimeout: 3000,
      navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
      responsive: {
        0: {
          items: 1,
          nav: false,
        },
        600: {
          items: 2,
          nav: false,
        },
        1024: {
          items: 3,
          nav: true,
        },
      }
    });

  }

  if ($('.customer-carousel').length) {
    $('.customer-carousel').owlCarousel({
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      loop: true,
      margin: 10,
      nav: true,
      dots: false,
      margin: 20,
      smartSpeed: 500,
      autoHeight: true,
      autoplay: true,
      autoplayTimeout: 3000,
      navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
      responsive: {
        0: {
          items: 1,
          nav: false,
        },
        600: {
          items: 2,
          nav: false,
        },
        1024: {
          items: 3,
          nav: true,
        },
      }
    });

  }
  if ($('.product-carousel').length) {
    $('.product-carousel').owlCarousel({
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      loop: true,
      margin: 10,
      nav: true,
      dots: false,
      margin: 20,
      smartSpeed: 500,
      autoHeight: true,
      autoplay: true,
      autoplayTimeout: 3000,
      navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
      responsive: {
        0: {
          items: 1,
          nav: false,
        },
        600: {
          items: 2,
          nav: false,
        },
        1024: {
          items: 3,
          nav: true,
        },
      }
    });

  }

});