<?php 
/** Template Name: Trang giới thiệu
*/

$context            =   Timber::context();
$post               =   new Timber\Post();
$context['post']    =   $post;
Timber::render( 'page-templates/intro.twig', $context );
