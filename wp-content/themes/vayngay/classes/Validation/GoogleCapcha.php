<?php
use Illuminate\Contracts\Validation\Rule;

class GoogleCapcha implements Rule
{
    public function __construct(){}

    public function passes($attribute, $value)
    {
        $recaptcha = new \ReCaptcha\ReCaptcha(get_option('options_recapcha_server'));
        $resp = $recaptcha->setExpectedHostname($_SERVER['SERVER_NAME'])
                          ->verify($value, $_SERVER['REMOTE_ADDR']);
        return $resp->isSuccess();
    }

    public function message()
    {
        return "Vui lòng tải lại trang trước khi gửi một yêu cầu mới.";
    }
}