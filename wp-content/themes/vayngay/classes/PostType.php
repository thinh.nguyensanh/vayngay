<?php
namespace Config;
class PostType
{
    public function __construct() {
		add_action( 'init', array( $this, 'loan_post_type' ), 1);
        add_action( 'init', array( $this, 'product_post_type' ), 1);
	}

    public function loan_post_type(){
        $labels = array(
            'name'                  => _x( 'Đăng ký vay', 'Post Type General Name', TEXTDOMAIN ),
            'singular_name'         => _x( 'Đăng ký vay', 'Post Type Singular Name', TEXTDOMAIN ),
            'menu_name'             => __( 'Đăng ký vay', TEXTDOMAIN ),
            'name_admin_bar'        => __( 'Đăng ký vay', TEXTDOMAIN ),
            'archives'              => __( 'Đăng ký vay', TEXTDOMAIN ),
            'attributes'            => __( 'Đăng ký vay', TEXTDOMAIN ),
            'parent_item_colon'     => __( 'Đăng ký vay', TEXTDOMAIN ),
            'all_items'             => __( 'Danh sách đăng ký', TEXTDOMAIN ),
            'add_new_item'          => __( 'Thêm mới', TEXTDOMAIN ),
            'add_new'               => __( 'Thêm mới', TEXTDOMAIN ),
		    'new_item'              => __( 'Thêm mới', TEXTDOMAIN ),
        );
        $rewrite = array(
            'slug'                  => 'loan',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => false,
        );
        $args = array(
            'label'                 => __( 'Đăng ký vay', TEXTDOMAIN ),
            'description'           => __( 'Đăng ký vay', TEXTDOMAIN ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'editor'),
            'hierarchical'          => false,
            'public'                => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 6,
            'menu_icon' 			=> 'dashicons-id',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => false,
            'capability_type'       => 'post', 
            'show_in_rest' 			=> false,
            'rewrite'               => $rewrite,
        );
        register_post_type( 'loan', $args );
    }
    
    public function product_post_type(){
        $labels = array(
            'name'                  => _x( 'Sản phẩm', 'Post Type General Name', TEXTDOMAIN ),
            'singular_name'         => _x( 'Sản phẩm', 'Post Type Singular Name', TEXTDOMAIN ),
            'menu_name'             => __( 'Sản phẩm', TEXTDOMAIN ),
            'name_admin_bar'        => __( 'Sản phẩm', TEXTDOMAIN ),
            'archives'              => __( 'Sản phẩm', TEXTDOMAIN ),
            'attributes'            => __( 'Sản phẩm', TEXTDOMAIN ),
            'parent_item_colon'     => __( 'Sản phẩm', TEXTDOMAIN ),
            'all_items'             => __( 'Danh sách sản phẩm', TEXTDOMAIN ),
            'add_new_item'          => __( 'Thêm sản phẩm', TEXTDOMAIN ),
            'add_new'               => __( 'Thêm sản phẩm', TEXTDOMAIN ),
		    'new_item'              => __( 'Thêm sản phẩm', TEXTDOMAIN ),
        );
        $rewrite = array(
            'slug'                  => 'san-pham',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => false,
        );
        $args = array(
            'label'                 => __( 'Sản phẩm', TEXTDOMAIN ),
            'description'           => __( 'Sản phẩm', TEXTDOMAIN ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'thumbnail', 'editor'),
            'taxonomies'            => array( 'product_category' ),
            'hierarchical'          => true,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 6,
            'menu_icon' 			=> 'dashicons-store',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'post',
            'show_in_rest' 			=> false,
            'rewrite'               => $rewrite
        );
        register_post_type( 'product', $args );
    }
}