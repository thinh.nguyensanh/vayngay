<?php
class ThemeConfig extends Timber\Site {

	public function __construct() {
		new Config\Enqueue;
		new Config\PostType;
		new Config\Taxonomy;
		parent::__construct();
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'after_setup_theme', array( $this, 'create_menu' ) );
	}

	public function create_menu(){
        register_nav_menus(array(
            'main' => esc_html__( 'Menu chính', TEXTDOMAIN ),
        ));
        register_nav_menus(array(
            'footer' => esc_html__( 'Menu footer', TEXTDOMAIN ),
        ));
		register_nav_menus(array(
            'product' => esc_html__( 'Menu sản phẩm', TEXTDOMAIN ),
        ));
    }

	public function add_to_context( $context ) {

		$context['menu_main']  = new Timber\Menu('main');
		$context['menu_footer']  = new Timber\Menu('footer');
		$context['menu_product']  = new Timber\Menu('product');
		$context['options']  = get_fields('options');
		$context['breadcrumb'] = $this->breadcrumb();
		$context['ajax_url'] = admin_url('admin-ajax.php');
		$start_year = date('Y',strtotime('-60 year', strtotime(current_time("Y"))));
    	$end_year = date('Y',strtotime('-18 year', strtotime(current_time("Y"))));
		$context['range_year'] = range($start_year, $end_year);
		$context['site']  = $this;
		
		return $context;
	}

	public function theme_supports() {
		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		// add_theme_support(
		// 	'html5',
		// 	array(
		// 		'comment-form',
		// 		'comment-list',
		// 		'gallery',
		// 		'caption',
		// 	)
		// );

		// add_theme_support(
		// 	'post-formats',
		// 	array(
		// 		'aside',
		// 		'image',
		// 		'video',
		// 		'quote',
		// 		'link',
		// 		'gallery',
		// 		'audio',
		// 	)
		// );

		add_theme_support( 'menus' );
	}

	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig\Extension\StringLoaderExtension() );
		$twig->addFilter( new Twig\TwigFilter( 'strpad', array( $this, 'strpad' ) ) );
		$twig->addFunction( new Timber\Twig_Function( 'callback_function' ) );
		return $twig;
	}

	public function strpad($number, $pad_length, $pad_string) {
		return str_pad($number, $pad_length, $pad_string, STR_PAD_LEFT);
	}

	public function breadcrumb(){
		$breadcrumb = [];
		$object = get_queried_object();
		if($object){
			switch(get_class($object)){
				case 'WP_Post':
					$post = new Timber\Post($object->ID);					
					$breadcrumb[] = [
						'link'	=> $post->link,
						'title' => $post->title
					];
					if($post->post_type === 'page'):
						$parent_page_id = $post->post_parent;
						while($parent_page_id != 0):
							$parent_page = new Timber\Post($parent_page_id);
							$breadcrumb[] = [
								'link'	=>	$parent_page->link,
								'title' =>	$parent_page->title
							];
							$parent_page_id = $parent_page->post_parent;
						endwhile;
					elseif($post->terms):
						foreach($post->terms as $term){
							if('language' != $term->taxonomy && 'post_translations' != $term->taxonomy){
								$real_term = $term;
							}
							if('language' == $term->taxonomy){
								$language_term = $term;
							}
						}
						$main_term = isset($real_term) ? $real_term : $language_term;
						$breadcrumb[] = [
							'link'	=>	$main_term->link,
							'title' =>	$main_term->name
						];
						$parent_term_id = $main_term->parent;
						while($parent_term_id != 0):
							$parent_term = new Timber\Term($parent_term_id);
							$breadcrumb[] = [
								'link'	=>	$parent_term->link,
								'title' =>	$parent_term->name
							];
							$parent_term_id = $parent_term->parent;
						endwhile;
					endif;
					break;
				case 'WP_Term':
					$current_term = new Timber\Term($object->term_id);
					$breadcrumb[] = [
						'link'	=>	$current_term->link,
						'title' =>	$current_term->name
					];
					$parent_term_id = $current_term->parent;
					while($parent_term_id != 0):
						$parent_term = new Timber\Term($parent_term_id);
						$breadcrumb[] = [
							'link'	=>	$parent_term->link,
							'title' =>	$parent_term->name
						];
						$parent_term_id = $parent_term->parent;
					endwhile;
					break;
				case 'WP_Post_Type':
					$breadcrumb[] = [
						'link'	=>	home_url($object->rewrite['slug']),
						'title' =>	$object->labels->name
					];
					break;
				case 'WP_User':
					$user = new Timber\User($object->ID);
					$breadcrumb[] = [
						'link'	=>	$user->link,
						'title' =>	$user->display_name
					];
					break;
				default:
					break;
			}
		}else{
			if(is_home()){
				$title = __( 'Homepage', TEXTDOMAIN );
			}elseif(is_404()){
				$title = __( '404', TEXTDOMAIN );
			}elseif(is_search()){
				$title = __( 'Search', TEXTDOMAIN );
			}elseif(is_day()){
				$title = get_the_date('d F Y');
			}elseif(is_month()){
				$title = get_the_date('F Y');
			}elseif(is_year()){
				$title = get_the_date('Y');
			}else{
				$title = __( 'Homepage', TEXTDOMAIN );
			}
			$breadcrumb[] = [
				'link'	=>	'#',
				'title' =>	$title
			];
		}
		
		$homepage = get_option('page_on_front');
		if($homepage){
			$homepage_title = get_the_title($homepage);
		}else{
			$homepage_title = get_bloginfo('title');
		}
		$breadcrumb[] = [
			'link'	=> home_url(),
			'title' => 'Trang chủ'
		];
		
		return array_reverse($breadcrumb);
	}
}