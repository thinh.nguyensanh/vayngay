<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/13/19
 * Time: 5:11 PM
 */
?>

<form id="form-search" method="get">
  <label for="inp" class="search-label">
    <input type="text" name="s" placeholder="&nbsp;">
    <span class="label">Tìm kiếm</span>
    <span class="search-border"></span>
    <button type="submit">
      <i class="fas fa-search"></i>
    </button>
  </label>
</form>
