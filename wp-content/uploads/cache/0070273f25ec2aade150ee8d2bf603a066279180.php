<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/15/19
 * Time: 3:00 PM
 */
?>

<div class="wrapper-partner">
  <div class="container">
    <div class="wrapper-title">
      <h3>ĐỐI TÁC</h3>
      <p>Digipay, đơn vị sở hữu Vayngay hiện đang là đối tác giới thiệu sản phẩm của</p>
    </div>

    <div class="wrapper-content">
      <div class="list-partner">
        <?php $__currentLoopData = $list_partner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $partner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="partner-logo">
            <img src="<?php echo e($partner['image']); ?>">
          </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    </div>
  </div>
</div>
