<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/14/19
 * Time: 10:36 AM
 */
?>

<div class="about-item <?php echo e($class); ?>">
  <div class="about-icon">
    <img src="<?php echo e($image); ?>">
  </div>
  <div class="about-title">
    <h4><?php echo e($title); ?></h4>
  </div>
  <div class="about-desc">
    <p><?php echo e($desc); ?></p>
  </div>
</div>
