<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/14/19
 * Time: 11:55 AM
 */
?>
<div class="wrapper-news">
  <div class="container">
    <div class="wrapper-title">
      <h3>TIN TỨC</h3>
      <p>Cập nhật những thông tin về tài chính cùng Vayngay</p>
    </div>

    <div class="wrapper-content">
      <div class="list-news">
        <?php $__currentLoopData = $lasted_posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <?php $__env->startComponent('components.post-item-vertical', $post); ?>
          <?php echo $__env->renderComponent(); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
      <div class="btn-show-more">
        <a href="<?php echo e('/tin-tuc'); ?>" class="btn-vn-primary">Xem thêm</a>
      </div>
    </div>
  </div>
</div>
