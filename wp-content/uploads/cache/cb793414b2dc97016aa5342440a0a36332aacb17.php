<?php $__env->startSection('content'); ?>
  <?php $__env->startComponent('components.topic-title', [
  'title' => App::title()
  ]); ?>
  <?php echo $__env->renderComponent(); ?>

  <?php if(!have_posts()): ?>
    <div class="alert alert-warning">
      <?php echo e(__('Sorry, no results were found.', 'sage')); ?>

    </div>
    <?php echo get_search_form(false); ?>

  <?php endif; ?>

  <div class="list-posts">
    <div class="row py-4">
      <?php while(have_posts()): ?>
        <?php
          the_post();
          $id = get_the_ID();
          $featured_image_id = get_post_thumbnail_id($id);
          $category_detail = get_the_category($id);
        ?>
        <div class="col-md-6 col-12 px-0">
          <?php $__env->startComponent('components.post-item-vertical', [
              'name' => get_the_title(),
              'excerpt' => wp_trim_words( get_the_content(), 40, '...' ),
              'url' => get_the_permalink(),
              'image' => $featured_image_id ? wp_get_attachment_url($featured_image_id) : '',
              'views' => get_field('views', $id),
              'category_name' => !empty($category_detail) ? $category_detail[0]->name : '',
              'post_date' => date("d/m/Y", strtotime($post->post_date)),
          ]); ?>
          <?php echo $__env->renderComponent(); ?>
        </div>
      <?php endwhile; ?>
    </div>
  </div>
  <?php echo e(\App\Controllers\App::pagination_link()); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.layout-archive', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>