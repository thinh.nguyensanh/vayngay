<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/13/19
 * Time: 3:09 PM
 */
?>

<div class="container">
  <div class="wrapper-loan">
    <div class="wrapper-title">
      <h3>
        Bạn đang có nhu cầu?
      </h3>
    </div>
    <div class="wrapper-content">
      <div class="list-product">
        <div class="row">
          <?php $__currentLoopData = $list_product_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $product_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php
              if ($index == 0) {
                $product_item['class'] = 'active';
              }
            ?>
            <div class="col-6 col-md-6 col-lg-4 py-lg-0 py-3">
              <?php $__env->startComponent('components.product-item', $product_item); ?>
              <?php echo $__env->renderComponent(); ?>
            </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
      </div>

      <div class="form-fill-info">
        <form id="form-prescoring" method="post">
          <div class="form-title">
            <h4>Hãy nhập thông tin</h4>
          </div>

          <div class="form-content">
            <div class="form-group">
              <label>
                Họ và tên
              </label>
              <input type="text" name="fullName" placeholder="Trương Đình Công">
            </div>

            <div class="form-group">
              <label>
                Số điện thoại
              </label>
              <input type="text" name="phoneNumber" placeholder="090 345 xxxx">
            </div>

            <div class="form-group">
              <label>
                Số chứng minh
              </label>
              <input type="text" name="idNumber" placeholder="025 xx xx xx">
            </div>

            <div id="group-address" class="form-group">
              <label>
                Địa chỉ
              </label>

              <select class="single-select" name="address">
                <option value=""></option>
                <?php $__currentLoopData = $list_district; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $district): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($district['code']); ?>"><?php echo e($district['name']); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
            </div>

            <div class="form-group" style="text-align: center">
              <input type="hidden" name="project" value="1">
              <?php $__env->startComponent('components.button', [
                'type'=> 'submit',
                'id' => 'btn-loan-reg',
                'text' => 'Đăng ký',
                'class' => 'btn-vn-primary',
                'extra' => 'data-style=expand-right',
              ]); ?>
              <?php echo $__env->renderComponent(); ?>

            </div>

            <div class="notice notice--check-loan">
              <div class="notice--check-loan__title">
              </div>
              <div class="notice--check-loan__content">
              </div>
            </div>
          </div>
        </form>

        <form id="form-final" method="post" style="display: none">
          <div class="form-content">
            <div class="form-group">
              <label>
                Hình thức vay
              </label>
              <select class="single-select" name="productCode">
                <option value=""></option>
                <?php $__currentLoopData = $list_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($product['code']); ?>"><?php echo e($product['name']); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
            </div>

            <div class="form-group">
              <label>
                Thu nhập
              </label>
              <select class="single-select" name="income">
                <option value=""></option>
                <?php $__currentLoopData = $list_income; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $income): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($income['code']); ?>"><?php echo e($income['name']); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
            </div>

            <div class="form-group">
              <label>
                Khoản vay mong muốn
              </label>
              <select class="single-select" name="loanAmount">
                <option value=""></option>
                <?php $__currentLoopData = $list_loan_amount; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loan_amount): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($loan_amount['code']); ?>"><?php echo e($loan_amount['name']); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
            </div>

            <div class="form-group">
              <label>
                Thời hạn vay mong muốn
              </label>
              <select class="single-select" name="loanTerm">
                <option value=""></option>
                <?php $__currentLoopData = $list_loan_term; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loan_term): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($loan_term['code']); ?>"><?php echo e($loan_term['name']); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
            </div>

            <div class="form-group" style="text-align: center">
              <?php $__env->startComponent('components.button', [
                'type'=> 'submit',
                'id' => 'btn-loan-final',
                'text' => 'Hoàn tất',
                'class' => 'btn-vn-primary',
                'extra' => 'data-style=expand-right',
              ]); ?>
              <?php echo $__env->renderComponent(); ?>
            </div>

          </div>
        </form>
      </div>
    </div>
  </div>
</div>
