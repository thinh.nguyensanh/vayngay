<article <?php post_class() ?>>
  <header class="entry-header">
    <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>">
    <?php
      $category_detail = get_the_category();
      foreach ($category_detail as $category) $list_name_category[] = $category->name;
    ?>
    <h3 class="entry-category">
      <?php echo implode(',', $list_name_category); ?>

    </h3>
    <h1 class="entry-title"><?php echo get_the_title(); ?></h1>
    <?php echo $__env->make('partials.single.entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </header>

  <div class="entry-content">
    <?php the_content() ?>
  </div>

  <footer class="entry-footer">
    <?php
      $tags = get_the_tags();
    ?>
    <?php if(!empty($tags)): ?>
      <div class="entry-tag">
        <p>Tags</p>
        <?php $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <?php $__env->startComponent('components.tag', [
            'tag' => $tag,
          ]); ?>
          <?php echo $__env->renderComponent(); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    <?php endif; ?>
  </footer>
</article>

<div class="related-post">
  <?php $__env->startComponent('components.topic-title', [
    'title' => 'Bài viết liên quan'
  ]); ?>
  <?php echo $__env->renderComponent(); ?>

  <div class="related-list-post">
    <div class="row py-5">
      <?php $__currentLoopData = $related_posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-6 col-12 px-0">
          <?php $__env->startComponent('components.post-item-vertical', $post); ?>
          <?php echo $__env->renderComponent(); ?>
        </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
  </div>
</div>
