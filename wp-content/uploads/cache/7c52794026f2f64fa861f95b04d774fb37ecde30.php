<div class="widget-group">
  <?php $__env->startComponent('components.topic-title', [
    'title' => 'Phổ biến'
  ]); ?>
  <?php echo $__env->renderComponent(); ?>

  <?php $__currentLoopData = $popular_posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="col-md-12 col-12 px-0">
      <?php $__env->startComponent('components.post-item-horizontal', $post); ?>
      <?php echo $__env->renderComponent(); ?>
    </div>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<div class="widget-group">
  <?php $__env->startComponent('components.topic-title', [
    'title' => 'Tags'
  ]); ?>
  <?php echo $__env->renderComponent(); ?>

  <div class="list-tags pt-3">
    <?php $__currentLoopData = $all_tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php $__env->startComponent('components.tag', [
        'tag' => $tag,
      ]); ?>
      <?php echo $__env->renderComponent(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
</div>

<div class="widget-group">
  <?php $__env->startComponent('components.topic-title', [
    'title' => 'Mục lục'
  ]); ?>
  <?php echo $__env->renderComponent(); ?>

  <div class="list-categories">
    <?php $__currentLoopData = $all_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li>
        <a href="<?php echo get_category_link($category->term_id); ?>"><?php echo e($category->name); ?></a>
      </li>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
</div>
