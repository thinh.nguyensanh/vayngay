<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/14/19
 * Time: 10:01 AM
 */
?>

  <div class="wrapper-about">
    <div class="container">
      <div class="wrapper-title wrapper-title-light">
        <h3>Giới thiệu</h3>
        <p>Lý do đặt niềm tin tại Vayngay.vn</p>
      </div>

      <div class="wrapper-content">
          <div class="list-about">
            <div class="row">
              <?php $__currentLoopData = $list_about_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-6 py-3">
                  <?php $__env->startComponent('components.about-item', $item); ?>
                  <?php echo $__env->renderComponent(); ?>
                </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
          </div>
      </div>
    </div>
  </div>
