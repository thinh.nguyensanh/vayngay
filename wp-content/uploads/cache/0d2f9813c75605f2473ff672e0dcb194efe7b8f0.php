<?php $__env->startSection('content'); ?>
  <?php while(have_posts()): ?> <?php the_post() ?>
  <?php $__env->startComponent('components.topic-title', [
    'title' => App::title()
  ]); ?>
  <?php echo $__env->renderComponent(); ?>
    <?php echo $__env->make('partials.content-page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.layout-right-sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>