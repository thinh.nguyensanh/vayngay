<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/13/19
 * Time: 5:11 PM
 */
?>

<style>
  .search-label {
    position: relative;
    margin: auto;
    width: 100%;
    max-width: 280px;
    margin-top: -15px;
  }
  .search-label button {
    border: none;
    background: none;
    position: absolute;
    right: 0;
    bottom: 0;
    color: #333333a3;
  }
  .search-label .label {
    position: absolute;
    top: 6px;
    left: 0;
    font-size: 14px;
    color: #9098a9;
    font-weight: 500;
    transform-origin: 0 0;
    transition: all 0.2s ease;
    z-index: -1;
  }
  .search-label .search-border {
    position: absolute;
    bottom: 0;
    left: 0;
    height: 2px;
    width: 100%;
    background: #07f!important;
    transform: scaleX(0);
    transform-origin: 0 0;
    transition: all 0.15s ease;
  }
  .search-label input {
    -webkit-appearance: none;
    width: 100%;
    border: 0;
    font-family: inherit;
    height: 35px;
    font-size: 14px;
    font-weight: 500;
    border-bottom: 2px solid #c8ccd4;
    background: none;
    border-radius: 0;
    color: #223254;
    transition: all 0.15s ease;
  }
  .search-label input:not(:placeholder-shown) + span {
    color: #5a667f;
    transform: translateY(-20px) scale(0.8);
  }
  .search-label input:focus {
    background: none;
    outline: none;
  }
  .search-label input:focus + span {
    color: #07f;
    transform: translateY(-20px) scale(0.8);
  }
  .search-label input:focus + span + .search-border {
    transform: scaleX(1);
  }
</style>

<form method="get" action="">
  <label for="inp" class="search-label">
    <input type="text" placeholder="&nbsp;">
    <span class="label">Tìm kiếm</span>
    <span class="search-border"></span>
    <button type="submit">
      <i class="fas fa-search"></i>
    </button>
  </label>
</form>
