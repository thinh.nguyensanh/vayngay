<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/15/19
 * Time: 11:45 AM
 */
?>

<div class="post-item post-vertical pb-4">
  <a href="<?php echo e($url); ?>}">
    <div class="post-image">
      <img src="<?php echo e($image); ?>">
      <p><?php echo e($category_name); ?></p>
    </div>
    <div class="post-title">
      <h4><?php echo e($name); ?></h4>
    </div>
    <div class="post-meta">
      <span class="post-date">
        <?php echo e($post_date); ?>

      </span>
      <?php if($views && $views > 0): ?>
        <span class="post-view">
          <?php echo e($views); ?> lượt xem
        </span>
      <?php endif; ?>
    </div>
    <?php if(isset($excerpt)): ?>
    <div class="post-excerpt">
      <?php echo e($excerpt); ?>

    </div>
    <?php endif; ?>
  </a>
</div>
