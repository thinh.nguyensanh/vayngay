<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/16/19
 * Time: 3:23 PM
 */
?>

<a class="tag-item" href="<?php echo get_tag_link($tag->term_id); ?>">
  <span><?php echo e($tag->name); ?></span>
</a>
