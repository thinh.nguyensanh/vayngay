<div class="widget-group">
  <?php $__env->startComponent('components.topic-title', [
    'title' => 'Tags'
  ]); ?>
  <?php echo $__env->renderComponent(); ?>

  <div class="list-tags pt-3">
    <?php $__currentLoopData = $all_tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php $__env->startComponent('components.tag', [
        'tag' => $tag,
      ]); ?>
      <?php echo $__env->renderComponent(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
</div>

<div class="widget-group">
  <?php $__env->startComponent('components.topic-title', [
    'title' => 'Mục lục'
  ]); ?>
  <?php echo $__env->renderComponent(); ?>

  <div class="list-categories">
    <?php $__currentLoopData = $all_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li>
        <a href="<?php echo get_category_link($category->term_id); ?>"><?php echo e($category->name); ?></a>
      </li>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
</div>
