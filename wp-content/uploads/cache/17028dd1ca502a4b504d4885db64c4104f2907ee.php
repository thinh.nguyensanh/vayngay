<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/13/19
 * Time: 11:35 AM
 */
?>



<?php $__env->startSection('content'); ?>

  <section id="slider">
    <?php $__env->startComponent('components.slider', [
      'list_slider' => $list_slider
    ]); ?>

    <?php echo $__env->renderComponent(); ?>
  </section>

  <?php $__env->startComponent('components.popup-success'); ?>
  <?php echo $__env->renderComponent(); ?>

  <section id="section-loan">
    <?php echo $__env->make('partials.form-loan', [
      'list_product_item' => $list_product_item,
      'list_district' => $list_district,
    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </section>

  <section id="section-about">
    <?php echo $__env->make('partials.home.about', [
      'list_about_item' => $list_about_item,
    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </section>

  <section id="section-news">
    <?php echo $__env->make('partials.home.news', [
      'lasted_posts' => $lasted_posts,
    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </section>

  <section id="section-partner">
    <?php echo $__env->make('partials.home.partner', [
      'list_partner' => $list_partner,
    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>