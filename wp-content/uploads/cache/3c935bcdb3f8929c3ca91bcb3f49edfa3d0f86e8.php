<?php
/**
 * Created by PhpStorm.
 * User: lydat
 * Date: 5/13/19
 * Time: 6:14 PM
 */
?>

<button id="<?php echo e($id); ?>" type="<?php echo e($type); ?>" class="ladda-button <?php echo e($class); ?>" <?php echo e(isset($extra) ? $extra : ''); ?>>
  <span class="ladda-label">
    <?php echo e($text); ?>

  </span>
</button>
