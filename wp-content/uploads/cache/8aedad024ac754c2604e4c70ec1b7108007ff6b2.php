<div class="entry-meta">
  <div class="entry-date">
    <time class="updated" datetime="<?php echo e(get_post_time('c', true)); ?>"><?php echo e(date("d.m.Y", strtotime(get_the_date()))); ?></time>
  </div>

  <?php
    $post_views = get_field('views');
  ?>

  <?php if($post_views && $post_views > 0): ?>
    <div class="entry-views">
      <span><?php echo e($post_views); ?> lượt xem</span>
    </div>
  <?php endif; ?>
</div>
