<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K7HK6P7"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="header">
  <div class="header-top">
    <a href="">
      <h2>
        Thương hiệu uy tín, minh bạch và tin cậy. Liên hệ ngay với Vayngay!
      </h2>
      <img src="<?= App\asset_path('images/home/icon-double-arrow.svg'); ?>">
    </a>
  </div>

  <div id="navbar" class="header-bottom">
    <nav class="navbar full-width-layout navbar-light navbar-expand-lg nav-fill w-100">

      <a class="navbar-brand brand" href="<?php echo e(home_url('/')); ?>">VAYNGAY</a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-collapse"
              aria-controls="nav-collapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon">☰</span>
      </button>
      <div class="collapse navbar-collapse" id="nav-collapse">
        <?php if(has_nav_menu('primary_navigation')): ?>
          <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav mr-auto w-100']); ?>

        <?php endif; ?>

        <?php echo $__env->make('partials.form-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>


    </nav>

  </div>

</header>
