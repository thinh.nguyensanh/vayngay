<!doctype html>
<html <?php echo get_language_attributes(); ?>>
<?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body <?php body_class() ?>>
<?php do_action('get_header') ?>
<?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="web-container" role="document">
  <div class="content container my-5">
    <div class="row py-2">
      <main class="main col-md-8 col-12">
        <?php echo $__env->yieldContent('content'); ?>
      </main>
      <aside class="sidebar col-md-4 col-12 pl-md-4">
        <?php echo $__env->make('partials.sidebar-archive', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </aside>
    </div>
  </div>
</div>
<?php do_action('get_footer') ?>
<?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php wp_footer() ?>
</body>
</html>
