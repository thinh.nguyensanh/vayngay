#docker run -d --name v2_vayngay -p 82:80 -v "$PWD":/var/www/html/ php:7.2-apache
docker run -d --name v2_vayngay -p 82:80 \
  -v ${PWD}/wp-content:/var/www/html/wp-content \
  -v ${PWD}/wp-config.php:/var/www/html/wp-config.php \
  wordpress:5.2.1-php7.2
